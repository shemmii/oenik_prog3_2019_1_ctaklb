﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Repository
{
    using System.Linq;

    /// <summary>
    /// Interface for the different repositories
    /// </summary>
    /// <typeparam name="TEntity">Represents a class</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Gets get every entity
        /// </summary>
        /// <returns>Returns every entity</returns>
        IQueryable<TEntity> All { get; }

        /// <summary>
        /// Modify
        /// </summary>
        /// <param name="uniqueId">Specify what to modify</param>
        /// <param name="whichAttribute">Specify which attribute to modify</param>
        void Modify(string uniqueId, int whichAttribute);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">Insert this</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Remove
        /// </summary>
        /// <param name="entity">Remove this</param>
        void Remove(string entity);
    }
}
