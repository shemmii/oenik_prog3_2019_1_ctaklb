﻿// <copyright file="MonitorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Repository
{
    using System;
    using System.Linq;
    using MonitorWebshop.Data;

    /// <summary>
    /// Repository for monitors
    /// </summary>
    public class MonitorRepository : IRepository<Monitor>
    {
        /// <summary>
        /// Initialization
        /// </summary>
        private readonly MonitorWebshopModel monitorEntities = new MonitorWebshopModel();

        /// <summary>
        /// Gets return every monitor
        /// </summary>
        /// <returns>Every monitor as IQueryable</returns>
        public IQueryable<Monitor> All => this.monitorEntities.Monitor.AsQueryable();

        //// Returns one specific monitor
        // public Monitor ReturnOne(string monitorId)
        // {
        //    Monitor monitor = monitorEntities.Monitor.Single(x => x.MONITOR_ID.Equals(monitorId));
        //    return monitor;
        // }

        /// <summary>
        /// Insert new monitor
        /// </summary>
        /// <param name="entity">This is the monitor that will be inserted</param>
        public void Insert(Monitor entity)
        {
            this.monitorEntities.Monitor.Add(entity);
            this.monitorEntities.SaveChanges();
            Console.WriteLine("Insert OK");
        }

        /// <summary>
        /// Modify monitor
        /// </summary>
        /// <param name="uniqueId">Modify monitor with this specific id</param>
        /// <param name="whichAttribute">Which attribute to modify</param>
        public void Modify(string uniqueId, int whichAttribute)
        {
            var entity = this.monitorEntities.Monitor.Single(x => x.MONITOR_ID.Equals(uniqueId));

            // MONITOR_ID MONITOR_NEV MONITOR_GYARTO MONITOR_LEIRAS MONITOR_ATLO MONITOR_AR
            switch (whichAttribute)
            {
                // Probably shouldn't modify the primary key
                // case 0:
                //    {
                //        Console.WriteLine("New MONITOR_ID: ");
                //        entity.MONITOR_ID = Console.ReadLine();
                //        break;
                //    }
                case 1:
                    {
                        Console.WriteLine("New MONITOR_NEV: ");
                        entity.MONITOR_NEV = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 2:
                    {
                        Console.WriteLine("New MONITOR_GYARTO: ");
                        entity.MONITOR_GYARTO = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 3:
                    {
                        Console.WriteLine("New MONITOR_LEIRAS: ");
                        entity.MONITOR_LEIRAS = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 4:
                    {
                        Console.WriteLine("New MONITOR_ATLO: ");
                        entity.MONITOR_ATLO = Convert.ToInt32(Console.ReadLine());
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 5:
                    {
                        Console.WriteLine("New MONITOR_AR: ");
                        entity.MONITOR_AR = Convert.ToInt32(Console.ReadLine());
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                default:
                    break;
            }
        }

        /// <summary>
        /// Remove monitor
        /// </summary>
        /// <param name="entity">Remove monitor with this specific id</param>
        public void Remove(string entity)
        {
            var toRemove = this.monitorEntities.Monitor.Single(x => x.MONITOR_ID.Equals(entity));
            this.monitorEntities.Monitor.Remove(toRemove);
            this.monitorEntities.SaveChanges();
            Console.WriteLine("Remove OK");
        }
    }
}
