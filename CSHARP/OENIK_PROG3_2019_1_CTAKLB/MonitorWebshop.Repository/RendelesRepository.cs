﻿// <copyright file="RendelesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Repository
{
    using System;
    using System.Linq;
    using MonitorWebshop.Data;

    /// <summary>
    /// Repository for the orders
    /// </summary>
    public class RendelesRepository : IRepository<Rendeles>
    {
        /// <summary>
        /// Initialization
        /// </summary>
        private readonly MonitorWebshopModel monitorEntities = new MonitorWebshopModel();

        /// <summary>
        /// Gets returns all the orders
        /// </summary>
        /// <returns>All the orders</returns>
        public IQueryable<Rendeles> All => this.monitorEntities.Rendeles.AsQueryable();

        /// <summary>
        /// Insert an order
        /// </summary>
        /// <param name="entity">Order entity to insert</param>
        public void Insert(Rendeles entity)
        {
            this.monitorEntities.Rendeles.Add(entity);
            this.monitorEntities.SaveChanges();
            Console.WriteLine("Insert OK");
        }

        /// <summary>
        /// Modify an order
        /// </summary>
        /// <param name="uniqueId">Specify which order to modify with the id</param>
        /// <param name="whichAttribute">Specify which attribute to modify</param>
        public void Modify(string uniqueId, int whichAttribute)
        {
            var entity = this.monitorEntities.Rendeles.Single(x => x.MEGRENDELES_ID.Equals(uniqueId));

            // MEGRENDELES_ID MONITOR_ID MEGRENDELES_IDEJE VASARLO_EMAIL MEGRENDELT_MENNYISEG SZALLITASI_IDO
            // MONITOR_NEV MONITOR_GYARTO
            switch (whichAttribute)
            {
                    // Probably shouldn't modify the primary key
                // case 0:
                //    {
                //        Console.WriteLine("New MEGRENDELES_ID: ");
                //        entity.MEGRENDELES_ID= Console.ReadLine();
                //        monitorEntities.SaveChanges();
                //        Console.WriteLine("Update OK");
                //        break;
                //    }
                case 1:
                    {
                        Console.WriteLine("New MONITOR_ID: ");
                        entity.MONITOR_ID = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 2:
                    {
                        Console.WriteLine("New MEGRENDELES_IDEJE: ");
                        entity.MEGRENDELES_IDEJE = Convert.ToDateTime(Console.ReadLine());
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 3:
                    {
                        Console.WriteLine("New VASARLO_EMAIL: ");
                        entity.VASARLO_EMAIL = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 4:
                    {
                        Console.WriteLine("New MEGRENDELT_MENNYISEG: ");
                        entity.MEGRENDELT_MENNYISEG = Convert.ToInt32(Console.ReadLine());
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 5:
                    {
                        Console.WriteLine("New SZALLITASI_IDO: ");
                        entity.SZALLITASI_IDO = Convert.ToInt32(Console.ReadLine());
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 6:
                    {
                        Console.WriteLine("New MONITOR_NEV: ");
                        entity.MONITOR_NEV = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 7:
                    {
                        Console.WriteLine("New MONITOR_GYARTO: ");
                        entity.MONITOR_GYARTO = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                default:
                    break;
            }
        }

        /// <summary>
        /// Remove an order
        /// </summary>
        /// <param name="entity">Specify which order to remove with this id</param>
        public void Remove(string entity)
        {
            var toRemove = this.monitorEntities.Rendeles.Single(x => x.MEGRENDELES_ID.Equals(entity));
            this.monitorEntities.Rendeles.Remove(toRemove);
            this.monitorEntities.SaveChanges();
            Console.WriteLine("Remove OK");
        }
    }
}
