﻿// <copyright file="VasarloRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Repository
{
    using System;
    using System.Linq;
    using MonitorWebshop.Data;

    /// <summary>
    /// Repository for buyers
    /// </summary>
    public class VasarloRepository : IRepository<Vasarlo>
    {
        /// <summary>
        /// Initialization
        /// </summary>
        private readonly MonitorWebshopModel monitorEntities = new MonitorWebshopModel();

        /// <summary>
        /// Gets returns every buyer
        /// </summary>
        /// <returns>Every buyer in an IQueryable</returns>
        public IQueryable<Vasarlo> All => this.monitorEntities.Vasarlo.AsQueryable();

        /// <summary>
        /// Insert a buyer
        /// </summary>
        /// <param name="entity">Buyer entity to insert</param>
        public void Insert(Vasarlo entity)
        {
            try
            {
                this.monitorEntities.Vasarlo.Add(entity);
                this.monitorEntities.SaveChanges();
                Console.WriteLine("Insert OK");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Modify a buyer
        /// </summary>
        /// <param name="uniqueId">Email address to specify a buyer (this is unique)</param>
        /// <param name="whichAttribute">Which attribute to modify</param>
        public void Modify(string uniqueId, int whichAttribute)
        {
            var entity = this.monitorEntities.Vasarlo.Single(x => x.VASARLO_EMAIL.Equals(uniqueId));

            // VASARLO_VEZETEKNEV VASARLO_KERESZTNEV VASARLO_EMAIL VASARLO_CIM
            switch (whichAttribute)
            {
                // Probably shouldn't modify the primary key
                // case 3:
                //    {
                //        Console.WriteLine("New VASARLO_EMAIL: ");
                //        entity.VASARLO_EMAIL = Console.ReadLine();
                //        this.monitorEntities.SaveChanges();
                //        Console.WriteLine("Update OK");
                //        break;
                //    }
                case 1:
                    {
                        Console.WriteLine("New VASARLO_VEZETEKNEV: ");
                        entity.VASARLO_VEZETEKNEV = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 2:
                    {
                        Console.WriteLine("New VASARLO_KERESZTNEV: ");
                        entity.VASARLO_KERESZTNEV = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 3:
                    {
                        Console.WriteLine("New VASARLO_CIM: ");
                        entity.VASARLO_CIM = Console.ReadLine();
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                case 4:
                    {
                        Console.WriteLine("New VASARLO_TELEFON: ");
                        entity.VASARLO_TELEFON = Convert.ToInt32(Console.ReadLine());
                        this.monitorEntities.SaveChanges();
                        Console.WriteLine("Update OK");
                        break;
                    }

                default:
                    break;
            }
        }

        /// <summary>
        /// Remove a buyer
        /// </summary>
        /// <param name="entity">Specify a buyer to remove</param>
        public void Remove(string entity)
        {
            var toRemove = this.monitorEntities.Vasarlo.Single(x => x.VASARLO_EMAIL.Equals(entity));
            this.monitorEntities.Vasarlo.Remove(toRemove);
            this.monitorEntities.SaveChanges();
            Console.WriteLine("Remove OK");
        }
    }
}
