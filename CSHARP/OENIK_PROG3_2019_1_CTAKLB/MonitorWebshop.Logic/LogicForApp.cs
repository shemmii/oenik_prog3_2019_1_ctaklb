﻿// <copyright file="LogicForApp.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Logic
{
    using System;
    using System.Linq;
    using MonitorWebshop.Data;
    using MonitorWebshop.Repository;

    /// <summary>
    /// Logic class with all the operations - can be accessed by the ConsoleApp
    /// </summary>
    public class LogicForApp : ILogic
    {
        private readonly IRepository<Monitor> monitorRepo;
        private readonly IRepository<Vasarlo> buyerRepo;
        private readonly IRepository<Rendeles> orderRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicForApp"/> class.
        /// Init monitor repository
        /// </summary>
        /// <param name="monitorRepo">monitor repository</param>
        /// <param name="buyerRepo">buyer repository</param>
        /// <param name="rendelesRepo">order repository</param>
        public LogicForApp(
            IRepository<Monitor> monitorRepo,
            IRepository<Vasarlo> buyerRepo,
            IRepository<Rendeles> rendelesRepo)
        {
            this.monitorRepo = monitorRepo;
            this.buyerRepo = buyerRepo;
            this.orderRepo = rendelesRepo;
        }

        /// <summary>
        /// Gets list all orders
        /// </summary>
        /// <returns>All orders as IQueryable</returns>
        public IQueryable AllOrders
        {
            get
            {
                var q = from w in this.orderRepo.All
                        select w;
                return q;
            }
        }

        /// <summary>
        /// Gets list all buyers
        /// </summary>
        /// <returns>IQueryable with all the buyers</returns>
        public IQueryable AllBuyers
        {
            get
            {
                var q = from w in this.buyerRepo.All
                        select w;
                return q;
            }
        }

        /// <summary>
        /// Gets list all monitors
        /// </summary>
        /// <returns>IQueryable with all the monitors</returns>
        public IQueryable AllMonitors
        {
            get
            {
                var q = from w in this.monitorRepo.All
                        select w;
                return q;
            }
        }

        /// <summary>
        /// Insert a new monitor
        /// </summary>
        /// <param name="monitorId">Unique id of the monitor</param>
        /// <param name="monitorNev">Name of the monitor</param>
        /// <param name="monitorGyarto">Manufacturer of the monitor</param>
        /// <param name="monitorLeiras">Description of a monitor</param>
        /// <param name="monitorAtlo">Size of the monitor</param>
        /// <param name="monitorAR">Price of the monitor</param>
        public void AddMonitor(string monitorId, string monitorNev, string monitorGyarto, string monitorLeiras, int monitorAtlo, int monitorAR)
        {
            this.monitorRepo.Insert(
                new Monitor()
                {
                    MONITOR_ID = monitorId,
                    MONITOR_NEV = monitorNev,
                    MONITOR_GYARTO = monitorGyarto,
                    MONITOR_LEIRAS = monitorLeiras,
                    MONITOR_ATLO = monitorAtlo,
                    MONITOR_AR = monitorAR
                });
        }

        /// <summary>
        /// Remove a monitor
        /// </summary>
        /// <param name="whichToRemove">Specify which monitor to remove with the unique id</param>
        public void RemoveMonitor(string whichToRemove)
        {
            this.monitorRepo.Remove(whichToRemove);
        }

        /// <summary>
        /// Modify a monitor
        /// </summary>
        /// <param name="whichMonitor">Specify which monitor to modify with the unique id</param>
        /// <param name="whichAttributeToModify">Specify which attribute to modify with this number</param>
        public void ModifyMonitor(string whichMonitor, int whichAttributeToModify)
        {
            this.monitorRepo.Modify(whichMonitor, whichAttributeToModify);
        }

        /// <summary>
        /// Insert a new buyer
        /// </summary>
        /// <param name="email">Buyer's email</param>
        /// <param name="address">Buyer's address</param>
        /// <param name="firstName">Buyer's first name</param>
        /// <param name="secondName">Buyer's second name</param>
        /// <param name="phone">Buyer's phone number</param>
        public void AddBuyers(string email, string address, string firstName, string secondName, long phone)
        {
            this.buyerRepo.Insert(
                new Vasarlo()
                {
                    VASARLO_EMAIL = email,
                    VASARLO_CIM = address,
                    VASARLO_VEZETEKNEV = firstName,
                    VASARLO_KERESZTNEV = secondName,
                    VASARLO_TELEFON = phone
                });
        }

        /// <summary>
        /// Remove a buyer
        /// </summary>
        /// <param name="whichToRemove">Specify which buyer to remove with the unique email address</param>
        public void RemoveBuyer(string whichToRemove)
        {
            this.buyerRepo.Remove(whichToRemove);
        }

        /// <summary>
        ///  Modify a buyer
        /// </summary>
        /// <param name="buyerEmail">Specify which buyer to modify</param>
        /// <param name="whichAttribute">Specify which attribute of the buyer to modify</param>
        public void ModifyBuyer(string buyerEmail, int whichAttribute)
        {
            this.buyerRepo.Modify(buyerEmail, whichAttribute);
        }

        /// <summary>
        /// Add an order
        /// </summary>
        /// <param name="orderId">Id of the order</param>
        /// <param name="monitorId">Id of the monitor</param>
        /// <param name="buyerEmail">Email of the buyer</param>
        /// <param name="howMany">How many monitors are ordered</param>
        /// <param name="monitorName">Name of the monitor</param>
        /// <param name="monitorManufacturer">Manufacturer of the monitor</param>
        public void AddOrder(string orderId, string monitorId, string buyerEmail, int howMany, string monitorName, string monitorManufacturer)
        {
            DateTime orderTime = DateTime.Now;
            this.orderRepo.Insert(
                new Rendeles()
                {
                    SZALLITASI_IDO = 3,
                    MEGRENDELES_ID = orderId,
                    MONITOR_ID = monitorId,
                    MEGRENDELES_IDEJE = orderTime,
                    MEGRENDELT_MENNYISEG = howMany,
                    MONITOR_GYARTO = monitorManufacturer,
                    MONITOR_NEV = monitorName,
                    VASARLO_EMAIL = buyerEmail
                });
        }

        /// <summary>
        /// Remove an order
        /// </summary>
        /// <param name="whichToRemove">Specify which order to remove with order id</param>
        public void RemoveOrder(string whichToRemove)
        {
            this.orderRepo.Remove(whichToRemove);
        }

        /// <summary>
        /// Modify an order
        /// </summary>
        /// <param name="orderId">Specify which order to modify with order id</param>
        /// <param name="whichAttribute">Specify which attribute to modify</param>
        public void ModifyOrder(string orderId, int whichAttribute)
        {
            this.orderRepo.Modify(orderId, whichAttribute);
        }

        /// <summary>
        /// Return number of monitors by manufacturers
        /// </summary>
        /// <returns>Number of monitors by manufacturers</returns>
        public string ReturnMonitorByManufacturer()
        {
            var monitorByMan = from m in this.monitorRepo.All
                               group m by m.MONITOR_GYARTO into g
                               orderby g.Count() descending
                               select new
                               {
                                   MONITOR_MAN = g.Key,
                                   MONITOR_COUNT = g.Count()
                               };
            string what = string.Empty;
            foreach (var item in monitorByMan)
            {
                what += "Manufacturer: " + item.MONITOR_MAN + " \tCount: " + item.MONITOR_COUNT + " \n ";
            }

            return what;
        }

        /// <summary>
        /// Return buyers from the specified city
        /// </summary>
        /// <param name="where">Specify which city</param>
        /// <returns>Buyers from the specified city</returns>
        public string ReturnBuyersFrom(string where)
        {
            string what = string.Empty;

            var everyBuyer = this.buyerRepo.All;

            var everyBuyerByAddress = from m in everyBuyer
                                      where m.VASARLO_CIM.Contains(@where)
                                      select new
                                      {
                                          FIRST = m.VASARLO_VEZETEKNEV,
                                          SECOND = m.VASARLO_KERESZTNEV,
                                          ADDRESS = m.VASARLO_CIM
                                      };

            foreach (var item in everyBuyerByAddress)
            {
                what += item.FIRST + item.SECOND + "\tAddress: " + item.ADDRESS + " \n";
            }

            return what;
        }

        /// <summary>
        /// Returns average monitor price by manufacturers
        /// </summary>
        /// <returns>Average monitor price by manufacturers</returns>
        public string ReturnAverageMonitorPriceByManufacturer()
        {
            string what = string.Empty;
            var average = from m in this.monitorRepo.All
                          group m by m.MONITOR_GYARTO into g
                          select new
                          {
                              AVG = g.Average(x => x.MONITOR_AR),
                              MAN = g.Key
                          };
            foreach (var item in average)
            {
                what += "Manufacturer: " + item.MAN + "\tAverage price: " + item.AVG + " \n";
            }

            return what;
        }
    }
}
