﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for Logic
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets all monitors
        /// </summary>
        IQueryable AllMonitors { get; }

        /// <summary>
        /// Gets all orders
        /// </summary>
        IQueryable AllOrders { get; }

        /// <summary>
        /// Gets all buyers
        /// </summary>
        IQueryable AllBuyers { get; }

        /// <summary>
        /// Add a monitor
        /// </summary>
        /// <param name="monitorId">Unique id of the monitor</param>
        /// <param name="monitorNev">Name of the monitor</param>
        /// <param name="monitorGyarto">Manufacturer of the monitor</param>
        /// <param name="monitorLeiras">Description of the monitor</param>
        /// <param name="monitorAtlo">Size of the monitor</param>
        /// <param name="monitorAR">Price of the monitor</param>
        void AddMonitor(string monitorId, string monitorNev, string monitorGyarto, string monitorLeiras, int monitorAtlo, int monitorAR);

        /// <summary>
        /// Remove a monitor
        /// </summary>
        /// <param name="whichToRemove">Specify which monitor to remove</param>
        void RemoveMonitor(string whichToRemove);

        /// <summary>
        /// Modify a monitor
        /// </summary>
        /// <param name="whichMonitor">Specify which monitor to modify</param>
        /// <param name="whichAttributeToModify">Specify which attribute of the monitor to modify</param>
        void ModifyMonitor(string whichMonitor, int whichAttributeToModify);

        /// <summary>
        /// Insert a new buyer
        /// </summary>
        /// <param name="email">Email of the buyer</param>
        /// <param name="address">Address of the buyer</param>
        /// <param name="firstName">First name of the buyer</param>
        /// <param name="secondName">Second name of the buyer</param>
        /// <param name="phone">Phone number of the buyer</param>
        void AddBuyers(string email, string address, string firstName, string secondName, long phone);

        /// <summary>
        /// Remove a buyer
        /// </summary>
        /// <param name="whichToRemove">Specify which buyer to remove</param>
        void RemoveBuyer(string whichToRemove);

        /// <summary>
        /// Modify a buyer
        /// </summary>
        /// <param name="buyerEmail">Specify which buyer to modify</param>
        /// <param name="whichAttribute">Specify which attribute of the buyer to modify</param>
        void ModifyBuyer(string buyerEmail, int whichAttribute);

        /// <summary>
        /// Add a new order
        /// </summary>
        /// <param name="orderId">Unique order id</param>
        /// <param name="monitorId">Ordered monitor id</param>
        /// <param name="buyerEmail">Buyers email</param>
        /// <param name="howMany">How many monitors were ordered</param>
        /// <param name="monitorName">Monitor's name</param>
        /// <param name="monitorManufacturer">Monitor's manufacturer</param>
        void AddOrder(string orderId, string monitorId, string buyerEmail, int howMany, string monitorName, string monitorManufacturer);

        /// <summary>
        /// Remove an order
        /// </summary>
        /// <param name="whichToRemove">Specify which order to remove</param>
        void RemoveOrder(string whichToRemove);

        /// <summary>
        /// Modify an order
        /// </summary>
        /// <param name="orderId">Spedify which order to modify</param>
        /// <param name="whichAttribute">Specify which attribute of the order to modify</param>
        void ModifyOrder(string orderId, int whichAttribute);

        /// <summary>
        /// Return monitors by manufacturers
        /// </summary>
        /// <returns>Monitors by manufacturers</returns>
        string ReturnMonitorByManufacturer();

        /// <summary>
        /// Return buyers from a specific location
        /// </summary>
        /// <param name="where">Specify where</param>
        /// <returns>Buyers from a specific location</returns>
        string ReturnBuyersFrom(string where);

        /// <summary>
        /// Return average monitor price by manufacturer
        /// </summary>
        /// <returns>Average monitor price by manufacturer</returns>
        string ReturnAverageMonitorPriceByManufacturer();
    }
}
