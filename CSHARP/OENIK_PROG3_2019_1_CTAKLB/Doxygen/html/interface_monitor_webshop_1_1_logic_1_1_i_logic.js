var interface_monitor_webshop_1_1_logic_1_1_i_logic =
[
    [ "AddBuyers", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#a0f38dad53d7bc2a7481d344699be34e4", null ],
    [ "AddMonitor", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#ab251b057b5194fd9b543baed55bab4af", null ],
    [ "AddOrder", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#aeea9485f87580cdc51d35fe0bcc88319", null ],
    [ "ModifyBuyer", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#a7a1df4c6393db0d97aba017d9b2365a4", null ],
    [ "ModifyMonitor", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#ace6ea26d456bbe8b67854e24dc9bd090", null ],
    [ "ModifyOrder", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#a6d53926416d6efab681a69d2501cc8d3", null ],
    [ "RemoveBuyer", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#af64a865744990a79d8c97fb0c41f362c", null ],
    [ "RemoveMonitor", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#aeee7fc8b7fbb6f46e1ad490ca7dc3d2f", null ],
    [ "RemoveOrder", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#ae876fbf6b4e3800c940172d81c66b5e7", null ],
    [ "ReturnAverageMonitorPriceByManufacturer", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#a3d2acf44f91f8606fb9f9be11cf7c772", null ],
    [ "ReturnBuyersFrom", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#a6a640aba97b2db93cf77f211a456e764", null ],
    [ "ReturnMonitorByManufacturer", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#a04b9616c03e9fd8a6a7cc1ad201f9f1c", null ],
    [ "AllBuyers", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#ab118ff472aad58e7dbc4820df1380b76", null ],
    [ "AllMonitors", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#ae30b308e6613a794ed12e596bd278ea7", null ],
    [ "AllOrders", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html#a8c15de15f27f43a180db9d41f9b4c204", null ]
];