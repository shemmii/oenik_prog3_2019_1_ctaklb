var dir_36667c82f0722ed9a54908e9cc9edc41 =
[
    [ "MonitorWebshop.ConsoleApp", "dir_8fc4751a880ce705957f555c3817ce6e.html", "dir_8fc4751a880ce705957f555c3817ce6e" ],
    [ "MonitorWebshop.Data", "dir_d30a3cc0edb7ff3f03979df3c53da5e3.html", "dir_d30a3cc0edb7ff3f03979df3c53da5e3" ],
    [ "MonitorWebshop.Logic", "dir_c030d255066c6cb8ac758a18096d9c74.html", "dir_c030d255066c6cb8ac758a18096d9c74" ],
    [ "MonitorWebshop.Logic.Tests", "dir_9c2205fd567a9cd8ab62b0a4ca6e4b34.html", "dir_9c2205fd567a9cd8ab62b0a4ca6e4b34" ],
    [ "MonitorWebshop.Repository", "dir_060c3a580ebf82c54061e491a84eb12c.html", "dir_060c3a580ebf82c54061e491a84eb12c" ],
    [ "MonitorWebshop.Repository.Tests", "dir_358c9a8c57e0bc38873af1f2aadc48e0.html", "dir_358c9a8c57e0bc38873af1f2aadc48e0" ]
];