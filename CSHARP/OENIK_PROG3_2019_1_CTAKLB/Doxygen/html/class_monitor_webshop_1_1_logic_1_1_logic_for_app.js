var class_monitor_webshop_1_1_logic_1_1_logic_for_app =
[
    [ "LogicForApp", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a65865f37ea2a290a06739a19740b3b47", null ],
    [ "AddBuyers", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a02020996bb9fe0977f4a731dd196b40b", null ],
    [ "AddMonitor", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a0d4c214bc21018f0a20f3b666c4eedb7", null ],
    [ "AddOrder", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a0da49c405b2df2d432f76128c1f157dd", null ],
    [ "ModifyBuyer", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a3912367c2efdc71fb6fa438ff4ae5d29", null ],
    [ "ModifyMonitor", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#aa449b049ea7bfd6bc6445ffcb09b28a9", null ],
    [ "ModifyOrder", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#ac79358d7a9de97f92ddfdcfd9b3486bf", null ],
    [ "RemoveBuyer", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a72951f04ecdfd1332da3d61f320d66d5", null ],
    [ "RemoveMonitor", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a6352d5a8c795142cc54f55bee8f3732e", null ],
    [ "RemoveOrder", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a4008a39ecc87b2530369ad460ce54fe2", null ],
    [ "ReturnAverageMonitorPriceByManufacturer", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#ae8a62132e853a5e7ec5ce359901bd6fb", null ],
    [ "ReturnBuyersFrom", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#ae2359e731fa5a4e7ca5676882a6298ee", null ],
    [ "ReturnMonitorByManufacturer", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a7e8b18fe2963b7e1a5904d33e6d2e359", null ],
    [ "AllBuyers", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a65ddf3f77727a8511f6f5ba1073fc15b", null ],
    [ "AllMonitors", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a1e957a7143548200888c8fe1b9e44a44", null ],
    [ "AllOrders", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html#a414fcafcaa0689250a9e274115bf863a", null ]
];