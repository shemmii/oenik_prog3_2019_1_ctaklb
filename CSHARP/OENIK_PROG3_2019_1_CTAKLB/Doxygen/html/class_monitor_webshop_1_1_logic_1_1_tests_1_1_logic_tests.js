var class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests =
[
    [ "Test_If_Buyer_Count_Is_Exatly_One", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a5fea9c0671d20690683c5e09efe2afc6", null ],
    [ "Test_If_Exactly_One_Buyer_Is_Inserted", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#ac428776a59f9fbb6670d6b5031b5708a", null ],
    [ "Test_If_Exactly_One_Monitor_Is_Inserted", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a66926b270c06f33edd500a12ac1895b4", null ],
    [ "Test_If_Exactly_One_Order_Is_Inserted", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#ac27f25791f51a536e275f5e3c547101d", null ],
    [ "Test_If_Monitor_Count_Is_Exactly_One", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a84886f9dbf550264b5cf9ecd6186b6ce", null ],
    [ "Test_If_No_New_Buyer_Is_Inserted", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a80143d4497c65ebe5a20e8fc81048a3f", null ],
    [ "Test_If_No_New_Monitor_Is_Inserted", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#ad09dd002866e975e859fe38085db6873", null ],
    [ "Test_If_No_New_Order_Is_Inserted", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#af55dea9497980932b9425fc55e00f4bc", null ],
    [ "Test_If_Order_Count_Is_Exatly_One", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#ac17b41832d9382a2cf30af6ea356f362", null ],
    [ "Test_Monitor_By_Manufacturer_1", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#afe8e15e162938ea938439376efba974c", null ],
    [ "Test_Monitor_By_Manufacturer_2", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a2cb42c78abbd1edaafa903af726e8581", null ],
    [ "Test_Monitor_By_Manufacturer_3", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a0891ca8c782e002bca3257189a2c5d49", null ],
    [ "Test_Return_Average_Monitor_Price_By_Manufacturer_1", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#ae5c06ab34cf541e72dc95cb103cc81c1", null ],
    [ "Test_Return_Average_Monitor_Price_By_Manufacturer_2", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a2a464954afa57f4754126af5edbc8713", null ],
    [ "Test_Return_Average_Monitor_Price_By_Manufacturer_3", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#ab3daf79e0de88817b9a698f53030ab25", null ],
    [ "Test_Return_Buyers_From_City_1", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a91305e8463109410d5ac26a5ad46d3e9", null ],
    [ "Test_Return_Buyers_From_City_2", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a110d14063f03c87d912e62617bb64f06", null ],
    [ "Test_Return_Buyers_From_City_3", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html#a0d67e3072e3506e74904638e004cc952", null ]
];