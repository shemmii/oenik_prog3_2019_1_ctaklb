var hierarchy =
[
    [ "MonitorWebshop.Repository.Tests.Class1", "class_monitor_webshop_1_1_repository_1_1_tests_1_1_class1.html", null ],
    [ "DbContext", null, [
      [ "MonitorWebshop.Data.MonitorWebshopModel", "class_monitor_webshop_1_1_data_1_1_monitor_webshop_model.html", null ]
    ] ],
    [ "MonitorWebshop.Logic.ILogic", "interface_monitor_webshop_1_1_logic_1_1_i_logic.html", [
      [ "MonitorWebshop.Logic.LogicForApp", "class_monitor_webshop_1_1_logic_1_1_logic_for_app.html", null ]
    ] ],
    [ "MonitorWebshop.Repository.IRepository< TEntity >", "interface_monitor_webshop_1_1_repository_1_1_i_repository.html", null ],
    [ "MonitorWebshop.Repository.IRepository< Monitor >", "interface_monitor_webshop_1_1_repository_1_1_i_repository.html", [
      [ "MonitorWebshop.Repository.MonitorRepository", "class_monitor_webshop_1_1_repository_1_1_monitor_repository.html", null ]
    ] ],
    [ "MonitorWebshop.Repository.IRepository< MonitorWebshop.Data.Monitor >", "interface_monitor_webshop_1_1_repository_1_1_i_repository.html", null ],
    [ "MonitorWebshop.Repository.IRepository< MonitorWebshop.Data.Rendeles >", "interface_monitor_webshop_1_1_repository_1_1_i_repository.html", null ],
    [ "MonitorWebshop.Repository.IRepository< MonitorWebshop.Data.Vasarlo >", "interface_monitor_webshop_1_1_repository_1_1_i_repository.html", null ],
    [ "MonitorWebshop.Repository.IRepository< Rendeles >", "interface_monitor_webshop_1_1_repository_1_1_i_repository.html", [
      [ "MonitorWebshop.Repository.RendelesRepository", "class_monitor_webshop_1_1_repository_1_1_rendeles_repository.html", null ]
    ] ],
    [ "MonitorWebshop.Repository.IRepository< Vasarlo >", "interface_monitor_webshop_1_1_repository_1_1_i_repository.html", [
      [ "MonitorWebshop.Repository.VasarloRepository", "class_monitor_webshop_1_1_repository_1_1_vasarlo_repository.html", null ]
    ] ],
    [ "MonitorWebshop.Logic.Tests.LogicTests", "class_monitor_webshop_1_1_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "MonitorWebshop.Data.Monitor", "class_monitor_webshop_1_1_data_1_1_monitor.html", null ],
    [ "MonitorWebshop.ConsoleApp.Program", "class_monitor_webshop_1_1_console_app_1_1_program.html", null ],
    [ "MonitorWebshop.Data.Rendeles", "class_monitor_webshop_1_1_data_1_1_rendeles.html", null ],
    [ "MonitorWebshop.Data.Vasarlo", "class_monitor_webshop_1_1_data_1_1_vasarlo.html", null ]
];