﻿DROP TABLE Rendeles;
DROP TABLE Monitor;
DROP TABLE Vasarlo;
CREATE TABLE Vasarlo
(
    VASARLO_VEZETEKNEV VARCHAR(30),
    VASARLO_KERESZTNEV VARCHAR(30),
    VASARLO_EMAIL VARCHAR(30),
    VASARLO_CIM VARCHAR(30),
    VASARLO_TELEFON  NUMERIC NOT NULL,
    CONSTRAINT PK_VASARLO PRIMARY KEY (VASARLO_EMAIL)
);

INSERT INTO Vasarlo VALUES('Nagy','Martin','mrphnn147@gmail.com','5000, Szolnok, Kajak utca 10.', 06305860311);
INSERT INTO Vasarlo VALUES('Kis','Ádám','kisa@gmail.com','5000, Szolnok, Sütő utca 2.', 06304864527);
INSERT INTO Vasarlo VALUES('Janicsák','Ilona','jilona@gmail.com','5000, Szolnok, Ponty utca 11.', 06208971548);
INSERT INTO Vasarlo VALUES('Valuska','Ágoston','vagoston@gmail.com','5000, Szolnok, Lepke utca 19.', 06708461231);
INSERT INTO Vasarlo VALUES('Réti','Virág','rvirag@gmail.com','5000, Szolnok, Abonyi út 66.', 06305845681);
INSERT INTO Vasarlo VALUES('Farkas','Bertalan','fbertalan@gmail.com','5000, Szolnok, Kacsa utca 22.', 06307892515);
INSERT INTO Vasarlo VALUES('Erdei','Fanni','efanni@gmail.com','5000, Szolnok, Fő utca 41.', 06305161486);



CREATE TABLE Monitor
(
	MONITOR_ID VARCHAR(30),
    MONITOR_NEV VARCHAR(30),
    MONITOR_GYARTO VARCHAR(30),
    MONITOR_LEIRAS VARCHAR(300),
    MONITOR_ATLO NUMERIC NOT NULL,
    MONITOR_AR NUMERIC NOT NULL,
    CONSTRAINT PK_MONITOR PRIMARY KEY (MONITOR_ID)
);

INSERT INTO Monitor VALUES('MON_ID001','BL3201PT','BENQ','BENQ BL3201PT típusú 32"-es Fekete színű monitor. Felbontás: 3840 x 2160, kontrasztarány: 1000:1, fényerő: 350 cd/m2, válaszidő: 4ms.',32,200000);
INSERT INTO Monitor VALUES('MON_ID002','RP55H','BENQ','BENQ RP552H Interactive Flat Panel típusú 55"-es Fekete színű monitor. Felbontás: 1920 x 1080 (Full HD), kontrasztarány: 1200:1, fényerő: 350 cd/m2, válaszidő: 6 ms. ',55,813000);
INSERT INTO Monitor VALUES('MON_ID003','TD2421','VIEWSONIC','VIEWSONIC TD2421 típusú 23,6"-es Fekete színű monitor. Felbontás: 1920 x 1080 (Full HD), kontrasztarány: 3000:1, fényerő: 250 cd/m2, válaszidő: 5 ms.  ',23.6,105000);
INSERT INTO Monitor VALUES('MON_ID004','P463','NEC','NEC MultiSync P463 típusú 46"-es Fekete színű monitor. Felbontás: 1920x1080, kontrasztarány: 4000:1, fényerő: 700 cd/m2, válaszidő: 8ms. ',46,773000);
INSERT INTO Monitor VALUES('MON_ID005','U3014','DELL','DELL U3014 típusú 30"-es Fekete színű monitor. Felbontás: 2560x1600, kontrasztarány: 1000:1, fényerő: 350 cd/m2, válaszidő: 6ms. ',30,200000);
INSERT INTO Monitor VALUES('MON_ID006','UN551S','NEC','NEC UN551S típusú 55"-es Fekete színű monitor. Felbontás: 1920 x 1080 (Full HD), kontrasztarány: 1200:1, fényerő: 500 cd/m2, válaszidő: 8 ms. ',55,2671000);
INSERT INTO Monitor VALUES('MON_ID007','4202L','ELO','ELO 4202L típusú 42"-es Fekete színű monitor. Felbontás: 1920 x 1080 (Full HD), kontrasztarány: 4000:1, fényerő: 500 cd/m2, válaszidő: 8 ms.',42,420000);


CREATE TABLE Rendeles
(
    MEGRENDELES_ID VARCHAR(30),
	MONITOR_ID VARCHAR(30),
    MEGRENDELES_IDEJE DATE NOT NULL,
    VASARLO_EMAIL VARCHAR(30),
    MEGRENDELT_MENNYISEG NUMERIC NOT NULL,
    SZALLITASI_IDO NUMERIC NOT NULL,
    MONITOR_NEV VARCHAR(30),
    MONITOR_GYARTO VARCHAR(30),
    CONSTRAINT PK_OSSZ PRIMARY KEY (MEGRENDELES_ID),
    CONSTRAINT FK_monitor FOREIGN KEY (MONITOR_ID)
        REFERENCES Monitor (MONITOR_ID),
    CONSTRAINT FK_vasarloemail FOREIGN KEY (VASARLO_EMAIL) 
        REFERENCES Vasarlo (VASARLO_EMAIL)
);

INSERT INTO Rendeles VALUES('MEGR_ID001','MON_ID002','20160406','rvirag@gmail.com',10,3,'RP55H','BENQ');
INSERT INTO Rendeles VALUES('MEGR_ID002','MON_ID006','20160512','mrphnn147@gmail.com',1,2,'UN551S','NEC');
INSERT INTO Rendeles VALUES('MEGR_ID003','MON_ID005','20150302','vagoston@gmail.com',2,4,'U3014','DELL');
INSERT INTO Rendeles VALUES('MEGR_ID004','MON_ID001','20161112','kisa@gmail.com',1,3,'BL3201PT','BENQ');
