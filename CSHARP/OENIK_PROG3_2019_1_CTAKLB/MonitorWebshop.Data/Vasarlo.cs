// <copyright file="Vasarlo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vasarlo")]
    public partial class Vasarlo
    {

        public Vasarlo()
        {
            this.Rendeles = new HashSet<Rendeles>();
        }

        [StringLength(30)]
        public string VASARLO_VEZETEKNEV { get; set; }

        [StringLength(30)]
        public string VASARLO_KERESZTNEV { get; set; }

        [Key]
        [StringLength(30)]
        public string VASARLO_EMAIL { get; set; }

        [StringLength(30)]
        public string VASARLO_CIM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VASARLO_TELEFON { get; set; }

        public virtual ICollection<Rendeles> Rendeles { get; set; }
    }
}
