// <copyright file="Rendeles.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rendeles
    {
        [Key]
        [StringLength(30)]
        public string MEGRENDELES_ID { get; set; }

        [StringLength(30)]
        public string MONITOR_ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime MEGRENDELES_IDEJE { get; set; }

        [StringLength(30)]
        public string VASARLO_EMAIL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MEGRENDELT_MENNYISEG { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SZALLITASI_IDO { get; set; }

        [StringLength(30)]
        public string MONITOR_NEV { get; set; }

        [StringLength(30)]
        public string MONITOR_GYARTO { get; set; }

        public virtual Monitor Monitor { get; set; }

        public virtual Vasarlo Vasarlo { get; set; }
    }
}
