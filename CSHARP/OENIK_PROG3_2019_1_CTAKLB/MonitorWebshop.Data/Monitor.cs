// <copyright file="Monitor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Monitor")]
    public partial class Monitor
    {
        public Monitor()
        {
            this.Rendeles = new HashSet<Rendeles>();
        }

        [Key]
        [StringLength(30)]
        public string MONITOR_ID { get; set; }

        [StringLength(30)]
        public string MONITOR_NEV { get; set; }

        [StringLength(30)]
        public string MONITOR_GYARTO { get; set; }

        [StringLength(300)]
        public string MONITOR_LEIRAS { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MONITOR_ATLO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MONITOR_AR { get; set; }

        public virtual ICollection<Rendeles> Rendeles { get; set; }
    }
}
