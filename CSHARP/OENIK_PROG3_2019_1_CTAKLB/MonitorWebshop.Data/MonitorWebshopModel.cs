// <copyright file="MonitorWebshopModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Data
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public partial class MonitorWebshopModel : DbContext
    {
        public MonitorWebshopModel()
            : base("name=MonitorWebshopModel")
        {
        }

        public virtual DbSet<Monitor> Monitor { get; set; }

        public virtual DbSet<Rendeles> Rendeles { get; set; }

        public virtual DbSet<Vasarlo> Vasarlo { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Monitor>()
                .Property(e => e.MONITOR_ID)
                .IsUnicode(false);

            modelBuilder.Entity<Monitor>()
                .Property(e => e.MONITOR_NEV)
                .IsUnicode(false);

            modelBuilder.Entity<Monitor>()
                .Property(e => e.MONITOR_GYARTO)
                .IsUnicode(false);

            modelBuilder.Entity<Monitor>()
                .Property(e => e.MONITOR_LEIRAS)
                .IsUnicode(false);

            modelBuilder.Entity<Monitor>()
                .Property(e => e.MONITOR_ATLO)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Monitor>()
                .Property(e => e.MONITOR_AR)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.MEGRENDELES_ID)
                .IsUnicode(false);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.MONITOR_ID)
                .IsUnicode(false);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.VASARLO_EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.MEGRENDELT_MENNYISEG)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.SZALLITASI_IDO)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.MONITOR_NEV)
                .IsUnicode(false);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.MONITOR_GYARTO)
                .IsUnicode(false);

            modelBuilder.Entity<Vasarlo>()
                .Property(e => e.VASARLO_VEZETEKNEV)
                .IsUnicode(false);

            modelBuilder.Entity<Vasarlo>()
                .Property(e => e.VASARLO_KERESZTNEV)
                .IsUnicode(false);

            modelBuilder.Entity<Vasarlo>()
                .Property(e => e.VASARLO_EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Vasarlo>()
                .Property(e => e.VASARLO_CIM)
                .IsUnicode(false);

            modelBuilder.Entity<Vasarlo>()
                .Property(e => e.VASARLO_TELEFON)
                .HasPrecision(18, 0);
        }
    }
}
