﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MonitorWebshop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests
    /// </summary>
    [TestFixture]
    internal class LogicTests
    {
        /// <summary>
        /// Test if monitor count is exactly one
        /// </summary>
        [Test]
        public void Test_If_Monitor_Count_Is_Exactly_One()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonM", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" }
            };
            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            // ACT
            var result = (IQueryable<Data.Monitor>)logic.AllMonitors;

            // ASSERT
            Assert.That(result.Count(), Is.EqualTo(1));
        }

        /// <summary>
        /// Test if buyer count is exactly one
        /// </summary>
        [Test]
        public void Test_If_Buyer_Count_Is_Exatly_One()
        {
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            List<Data.Vasarlo> buyers = new List<Data.Vasarlo>()
            {
                new Data.Vasarlo()
                {
                    VASARLO_CIM = "kamucim", VASARLO_EMAIL = "m@m.hu", VASARLO_KERESZTNEV = "kereszt",
                VASARLO_VEZETEKNEV = "vezetek", VASARLO_TELEFON = 36555
                }
            };
            buyerMockRepo.Setup(repo => repo.All).Returns(buyers.AsQueryable());

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            var result = (IQueryable<Data.Vasarlo>)logic.AllBuyers;

            Assert.That(result.Count(), Is.EqualTo(1));
        }

        /// <summary>
        /// Test if order count is exactly one
        /// </summary>
        [Test]
        public void Test_If_Order_Count_Is_Exatly_One()
        {
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            List<Data.Rendeles> orders = new List<Data.Rendeles>()
            {
                new Data.Rendeles()
                {
                    VASARLO_EMAIL = "m@m.hu", MEGRENDELES_ID = "kamuid_001", MEGRENDELES_IDEJE = DateTime.Now, MEGRENDELT_MENNYISEG = 5,
                MONITOR_GYARTO = "kamugyártó", MONITOR_ID = "MonID0475", SZALLITASI_IDO = 3, MONITOR_NEV = "MonNev"
                }
            };
            orderMockRepo.Setup(repo => repo.All).Returns(orders.AsQueryable());

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            var result = (IQueryable<Data.Rendeles>)logic.AllOrders;

            Assert.That(result.Count(), Is.EqualTo(1));
        }

        /// <summary>
        /// Test if no new monitor is inserted
        /// </summary>
        [Test]
        public void Test_If_No_New_Monitor_Is_Inserted()
        {
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonM", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" }
            };
            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            monitorMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Monitor>()), Times.Never);
        }

        /// <summary>
        /// Test if no new buyer is inserted
        /// </summary>
        [Test]
        public void Test_If_No_New_Buyer_Is_Inserted()
        {
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();

            List<Data.Vasarlo> buyers = new List<Data.Vasarlo>()
            {
                new Data.Vasarlo()
                {
                    VASARLO_CIM = "kamucim", VASARLO_EMAIL = "m@m.hu", VASARLO_KERESZTNEV = "kereszt",
                VASARLO_VEZETEKNEV = "vezetek", VASARLO_TELEFON = 36555
                }
            };
            buyerMockRepo.Setup(repo => repo.All).Returns(buyers.AsQueryable());

            buyerMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Vasarlo>()), Times.Never);
        }

        /// <summary>
        /// Test if no new order is inserted
        /// </summary>
        [Test]
        public void Test_If_No_New_Order_Is_Inserted()
        {
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            List<Data.Rendeles> orders = new List<Data.Rendeles>()
            {
                new Data.Rendeles()
                {
                    VASARLO_EMAIL = "m@m.hu", MEGRENDELES_ID = "kamuid_001", MEGRENDELES_IDEJE = DateTime.Now, MEGRENDELT_MENNYISEG = 5,
                MONITOR_GYARTO = "kamugyártó", MONITOR_ID = "MonID0475", SZALLITASI_IDO = 3, MONITOR_NEV = "MonNev"
                }
            };
            orderMockRepo.Setup(repo => repo.All).Returns(orders.AsQueryable());

            orderMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Rendeles>()), Times.Never);
        }

        /// <summary>
        /// Test if exactly one monitor is inserted
        /// </summary>
        [Test]
        public void Test_If_Exactly_One_Monitor_Is_Inserted()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            // ACT
            logic.AddMonitor("monid", "monnev", "mongy", "monle", 11, 500);

            // ASSERT
            monitorMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Monitor>()), Times.Once);
        }

        /// <summary>
        /// Test if exactly one buyer is inserted
        /// </summary>
        [Test]
        public void Test_If_Exactly_One_Buyer_Is_Inserted()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            // ACT
            logic.AddBuyers("m@m.hu", "kamuaddress", "veznev", "kernev", 36555);

            // ASSERT
            buyerMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Vasarlo>()), Times.Once);
        }

        /// <summary>
        /// Test if exactly one order is inserted
        /// </summary>
        [Test]
        public void Test_If_Exactly_One_Order_Is_Inserted()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            // ACT
            logic.AddOrder("kamuid", "monid", "m@m.hu", 5, "monnev", "monman");

            // ASSERT
            orderMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Rendeles>()), Times.Once);
        }

        /// <summary>
        /// Test if monitor count is ok
        /// </summary>
        [Test]
        public void Test_Monitor_By_Manufacturer_1()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonM", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" }
            };

            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            string[] splitted = logic.ReturnMonitorByManufacturer().Split(' ');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "MonM")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(1));
        }

        /// <summary>
        /// Test if monitor count is ok
        /// </summary>
        [Test]
        public void Test_Monitor_By_Manufacturer_2()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonM", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" },
                new Data.Monitor() { MONITOR_NEV = "MonN2", MONITOR_GYARTO = "NemMonM", MONITOR_ATLO = 110, MONITOR_AR = 5000, MONITOR_ID = "MONID_005", MONITOR_LEIRAS = "MonD" }
            };

            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            string[] splitted = logic.ReturnMonitorByManufacturer().Split(' ');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "MonM")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(1));
        }

        /// <summary>
        /// Test if monitor count is ok
        /// </summary>
        [Test]
        public void Test_Monitor_By_Manufacturer_3()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonMmmm", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" },
                new Data.Monitor() { MONITOR_NEV = "MonN2", MONITOR_GYARTO = "NemMonM", MONITOR_ATLO = 110, MONITOR_AR = 5000, MONITOR_ID = "MONID_005", MONITOR_LEIRAS = "MonD" }
            };

            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            string[] splitted = logic.ReturnMonitorByManufacturer().Split(' ');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "MonM")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(0));
        }

        /// <summary>
        /// Test if it returns the correct number of buyers from a specified city
        /// </summary>
        [Test]
        public void Test_Return_Buyers_From_City_1()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Vasarlo> buyers = new List<Data.Vasarlo>()
            {
                new Data.Vasarlo()
                {
                    VASARLO_CIM = "kamucim", VASARLO_EMAIL = "m@m.hu", VASARLO_KERESZTNEV = "kereszt",
                VASARLO_VEZETEKNEV = "vezetek", VASARLO_TELEFON = 36555
                }
            };
            buyerMockRepo.Setup(repo => repo.All).Returns(buyers.AsQueryable());

            string[] splitted = logic.ReturnBuyersFrom("kamucim").Split(' ');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "kamucim")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(1));
        }

        /// <summary>
        /// Test if it returns the correct number of buyers from a specified city
        /// </summary>
        [Test]
        public void Test_Return_Buyers_From_City_2()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Vasarlo> buyers = new List<Data.Vasarlo>()
            {
                new Data.Vasarlo()
                {
                    VASARLO_CIM = "kamucim", VASARLO_EMAIL = "m@m.hu", VASARLO_KERESZTNEV = "kereszt",
                VASARLO_VEZETEKNEV = "vezetek", VASARLO_TELEFON = 36555
                },
                new Data.Vasarlo()
                {
                    VASARLO_CIM = "kamucim", VASARLO_EMAIL = "m@m.hu", VASARLO_KERESZTNEV = "kereszt",
                VASARLO_VEZETEKNEV = "vezetek", VASARLO_TELEFON = 36555
                }
            };
            buyerMockRepo.Setup(repo => repo.All).Returns(buyers.AsQueryable());

            string[] splitted = logic.ReturnBuyersFrom("kamucim").Split(' ');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "kamucim")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(2));
        }

        /// <summary>
        /// Test if it returns the correct number of buyers from a specified city
        /// </summary>
        [Test]
        public void Test_Return_Buyers_From_City_3()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Vasarlo> buyers = new List<Data.Vasarlo>()
            {
                new Data.Vasarlo()
                {
                    VASARLO_CIM = "kamucim", VASARLO_EMAIL = "m@m.hu", VASARLO_KERESZTNEV = "kereszt",
                VASARLO_VEZETEKNEV = "vezetek", VASARLO_TELEFON = 36555
                },
                new Data.Vasarlo()
                {
                    VASARLO_CIM = "kamucim", VASARLO_EMAIL = "m@m.hu", VASARLO_KERESZTNEV = "kereszt",
                VASARLO_VEZETEKNEV = "vezetek", VASARLO_TELEFON = 36555
                }
            };
            buyerMockRepo.Setup(repo => repo.All).Returns(buyers.AsQueryable());

            string[] splitted = logic.ReturnBuyersFrom("kamucim1").Split(' ');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "kamucim")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(0));
        }

        /// <summary>
        /// Test if it returns the correct average monitor price
        /// </summary>
        [Test]
        public void Test_Return_Average_Monitor_Price_By_Manufacturer_1()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonMmmm", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" },
                new Data.Monitor() { MONITOR_NEV = "MonN2", MONITOR_GYARTO = "NemMonM", MONITOR_ATLO = 110, MONITOR_AR = 5000, MONITOR_ID = "MONID_005", MONITOR_LEIRAS = "MonD" }
            };

            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            string[] splitted = logic.ReturnAverageMonitorPriceByManufacturer().Split(' ', '.');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "500")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(1));
        }

        /// <summary>
        /// Test if it returns the correct average monitor price
        /// </summary>
        [Test]
        public void Test_Return_Average_Monitor_Price_By_Manufacturer_2()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonMmmm", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" },
                new Data.Monitor() { MONITOR_NEV = "MonN2", MONITOR_GYARTO = "NemMonM", MONITOR_ATLO = 110, MONITOR_AR = 5000, MONITOR_ID = "MONID_005", MONITOR_LEIRAS = "MonD" },
                new Data.Monitor() { MONITOR_NEV = "MonN2", MONITOR_GYARTO = "NemMonM", MONITOR_ATLO = 110, MONITOR_AR = 1000, MONITOR_ID = "MONID_006", MONITOR_LEIRAS = "MonD" }
            };

            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            string[] splitted = logic.ReturnAverageMonitorPriceByManufacturer().Split(' ', '.');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "3000")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(1));
        }

        /// <summary>
        /// Test if it returns the correct average monitor price
        /// </summary>
        [Test]
        public void Test_Return_Average_Monitor_Price_By_Manufacturer_3()
        {
            // ARRANGE
            Mock<IRepository<Data.Monitor>> monitorMockRepo = new Mock<IRepository<Data.Monitor>>();
            Mock<IRepository<Data.Vasarlo>> buyerMockRepo = new Mock<IRepository<Data.Vasarlo>>();
            Mock<IRepository<Data.Rendeles>> orderMockRepo = new Mock<IRepository<Data.Rendeles>>();

            LogicForApp logic = new LogicForApp(monitorMockRepo.Object, buyerMockRepo.Object, orderMockRepo.Object);

            List<Data.Monitor> monitors = new List<Data.Monitor>()
            {
                new Data.Monitor() { MONITOR_NEV = "MonN", MONITOR_GYARTO = "MonMmmm", MONITOR_ATLO = 11, MONITOR_AR = 500, MONITOR_ID = "MONID_007", MONITOR_LEIRAS = "MonD" },
                new Data.Monitor() { MONITOR_NEV = "MonN2", MONITOR_GYARTO = "NemMonM", MONITOR_ATLO = 110, MONITOR_AR = 5000, MONITOR_ID = "MONID_005", MONITOR_LEIRAS = "MonD" },
                new Data.Monitor() { MONITOR_NEV = "MonN2", MONITOR_GYARTO = "NemMonM", MONITOR_ATLO = 110, MONITOR_AR = 5000, MONITOR_ID = "MONID_006", MONITOR_LEIRAS = "MonD" }
            };

            monitorMockRepo.Setup(repo => repo.All).Returns(monitors.AsQueryable());

            string[] splitted = logic.ReturnAverageMonitorPriceByManufacturer().Split(' ', '.');
            int c = 0;
            foreach (var item in splitted)
            {
                if (item == "5000")
                {
                    c++;
                }
            }

            Assert.That(c, Is.EqualTo(1));
        }
    }
}
