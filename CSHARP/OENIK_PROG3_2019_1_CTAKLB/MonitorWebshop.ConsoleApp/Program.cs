﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MonitorWebshop.ConsoleApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MonitorWebshop.Logic;
    using MonitorWebshop.Repository;

    /// <summary>
    /// Program class
    /// </summary>
    internal class Program
    {
        private static readonly MonitorRepository MonitorsRepository = new MonitorRepository();
        private static readonly VasarloRepository BuyerRepository = new VasarloRepository();
        private static readonly RendelesRepository OrderRepository = new RendelesRepository();

        private static void Main()
        {
            while (true)
            {
                ShowMenu();
                TopLevelMenuChooser();
            }
        }

        /// <summary>
        /// Chose one from the menu
        /// </summary>
        private static void TopLevelMenuChooser()
        {
            Console.WriteLine("Choose one of the above (ex.: type 1 + press Enter)");
            int whichMenuPoint = Convert.ToInt16(Console.ReadLine());
            switch (whichMenuPoint)
            {
                case 0:
                    {
                        System.Environment.Exit(1);
                        break;
                    }

                case 1:
                    {
                        ListDb();
                        break;
                    }

                case 2:
                    {
                        AddItemToDb();
                        break;
                    }

                case 3:
                    {
                        DeleteItemFromDb();
                        break;
                    }

                case 4:
                    {
                        ModifyItemInDb();
                        break;
                    }

                case 5:
                    {
                        ComplexQueries();
                        break;
                    }

                default:
                    {
                        Console.WriteLine("No such menupoint - Exiting");
                        break;
                    }
            }
        }

        private static void ComplexQueries()
        {
            Console.Clear();
            ILogic logic = new LogicForApp(MonitorsRepository, BuyerRepository, OrderRepository);
            Console.WriteLine("1 --- List all monitors grouped by manufacturer");
            Console.WriteLine("2 --- List buyers from a specified city");
            Console.WriteLine("3 --- List average monitor price by manufacturer");
            Console.WriteLine("Choose one of the above (ex.: type 1 + press Enter)");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    {
                        Console.WriteLine(logic.ReturnMonitorByManufacturer());
                        break;
                    }

                case 2:
                    {
                        Console.WriteLine("What City: ");
                        Console.WriteLine(logic.ReturnBuyersFrom(Console.ReadLine()));
                        break;
                    }

                case 3:
                    {
                        Console.WriteLine(logic.ReturnAverageMonitorPriceByManufacturer());
                        break;
                    }

                default:
                    break;
            }
        }

        // Modify an item in a database
        private static void ModifyItemInDb()
        {
            Console.Clear();
            ILogic logic = new LogicForApp(MonitorsRepository, BuyerRepository, OrderRepository);
            Console.WriteLine("1 - Monitors");
            Console.WriteLine("2 - Buyers");
            Console.WriteLine("3 - Orders");

            Console.WriteLine("Choose one of the above (ex.: type 1 + press Enter)");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                    // Modify monitor
                case 1:
                    {
                        Console.WriteLine("ID of monitor you'd like to modify: ");
                        string monitorId = Console.ReadLine();
                        Console.WriteLine("Which attribute of a monitor would you like to modify?\n" +
                            "1 --- MONITOR_NEV\n" +
                            "2 --- MONITOR_GYARTO\n" +
                            "3 --- MONITOR_LEIRAS\n" +
                            "4 --- MONITOR_ATLO\n" +
                            "5 --- MONITOR_AR\n");
                        int whichOne = Convert.ToInt32(Console.ReadLine());
                        logic.ModifyMonitor(monitorId, whichOne);
                        break;
                    }

                    // Modify buyer
                case 2:
                    {
                        Console.WriteLine("Email of buyer to modify: ");
                        string buyerEmail = Console.ReadLine();
                        Console.WriteLine("Which attribute of a buyer would you like to modify?\n" +
                            "1 --- VASARLO_VEZETEKNEV\n" +
                            "2 --- VASARLO_KERESZTNEV\n" +
                            "3 --- VASARLO_EMAIL\n" +
                            "4 --- VASARLO_CIM\n" +
                            "5 --- VASARLO_TELEFON");
                        int whichOneToModify = Convert.ToInt32(Console.ReadLine());
                        logic.ModifyBuyer(buyerEmail, whichOneToModify);
                        break;
                    }

                    // Modify order
                case 3:
                    {
                        Console.WriteLine("ID of order to modify: ");
                        string orderId = Console.ReadLine();
                        Console.WriteLine("Which attribute of the order would you like to modify?\n" +
                            "1 --- MONITOR_ID\n" +
                            "2 --- MEGRENDELES_IDEJE\n" +
                            "3 --- VASARLO_EMAIL\n" +
                            "4 --- MEGRENDELT_MENNYISEG\n" +
                            "5 --- SZALLITASI_IDO\n" +
                            "6 --- MONITOR_NEV\n" +
                            "7 --- MONITOR_GYARTO");
                        int whichOneToModify = Convert.ToInt32(Console.ReadLine());
                        logic.ModifyOrder(orderId, whichOneToModify);
                        break;
                    }

                default:
                    break;
            }
        }

        // Remove an item from a database
        private static void DeleteItemFromDb()
        {
            Console.Clear();
            ILogic logic = new LogicForApp(MonitorsRepository, BuyerRepository, OrderRepository);
            Console.WriteLine("1 - Monitors");
            Console.WriteLine("2 - Buyers");
            Console.WriteLine("3 - Orders");

            Console.WriteLine("Choose one of the above (ex.: type 1 + press Enter)");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                // Remove monitor
                case 1:
                    Console.WriteLine("ID of monitor to delete from database: ");
                    string monitorId = Console.ReadLine();
                    logic.RemoveMonitor(monitorId);
                    break;

                // Remove buyer
                case 2:
                    Console.WriteLine("Email of buyer to remove from database: ");
                    string buyerEmail = Console.ReadLine();
                    logic.RemoveBuyer(buyerEmail);
                    break;

                // Remove order
                case 3:
                    Console.WriteLine("Id of order to remove from database: ");
                    string orderId = Console.ReadLine();
                    logic.RemoveOrder(orderId);
                    break;
                default:
                    break;
            }
        }

        // Add an item to one of the databases
        private static void AddItemToDb()
        {
            Console.Clear();
            ILogic logic = new LogicForApp(MonitorsRepository, BuyerRepository, OrderRepository);
            Console.WriteLine("1 - Monitors");
            Console.WriteLine("2 - Buyers");
            Console.WriteLine("3 - Orders");

            Console.WriteLine("Choose one of the above (ex.: type 1 + press Enter)");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                // Add new monitor
                case 1:
                    {
                        Console.WriteLine("Monitor ID: ");
                        string monitorId = Console.ReadLine();
                        Console.WriteLine("Monitor name: ");
                        string monitorName = Console.ReadLine();
                        Console.WriteLine("Monitor manufacturer: ");
                        string monitorManufacturer = Console.ReadLine();
                        Console.WriteLine("Monitor desc: ");
                        string monitorDesc = Console.ReadLine();
                        Console.WriteLine("Monitor size: ");
                        int monitorSize = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Monitor price: ");
                        int monitorPrice = Convert.ToInt32(Console.ReadLine());
                        logic.AddMonitor(monitorId, monitorName, monitorManufacturer, monitorDesc, monitorSize, monitorPrice);
                        break;
                    }

                    // Add new buyer
                case 2:
                    {
                        Console.WriteLine("Email: ");
                        string email = Console.ReadLine();
                        Console.WriteLine("Address: ");
                        string address = Console.ReadLine();
                        Console.WriteLine("First name: ");
                        string firstName = Console.ReadLine();
                        Console.WriteLine("Second name: ");
                        string secondName = Console.ReadLine();
                        Console.WriteLine("Phone: ");
                        long phone = Convert.ToInt64(Console.ReadLine());
                        logic.AddBuyers(email, address, firstName, secondName, phone);
                        break;
                    }

                    // Add new order
                case 3:
                    {
                        Console.WriteLine("Order ID: ");
                        string orderId = Console.ReadLine();
                        Console.WriteLine("Monitor ID:");
                        string monitorIdj = Console.ReadLine();
                        Console.WriteLine("Email: ");
                        string buyerEmail = Console.ReadLine();
                        Console.WriteLine("How many: ");
                        int howMany = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Monitor name: ");
                        string monitorNamej = Console.ReadLine();
                        Console.WriteLine("Monitor manufacturer: ");
                        string monMan = Console.ReadLine();
                        logic.AddOrder(orderId, monitorIdj, buyerEmail, howMany, monitorNamej, monMan);
                        break;
                    }

                default:
                    break;
            }
        }

        // List databases
        private static void ListDb()
        {
            Console.Clear();
            ILogic logic = new LogicForApp(MonitorsRepository, BuyerRepository, OrderRepository);
            Console.WriteLine("1 - Monitors");
            Console.WriteLine("2 - Buyers");
            Console.WriteLine("3 - Orders");

            Console.WriteLine("Choose one of the above (ex.: type 1 + press Enter)");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                // List monitors
                case 1:
                    var monitors = (IQueryable<Data.Monitor>)logic.AllMonitors;
                    monitors.ToList();
                    foreach (var item in monitors)
                    {
                        string fullMonitor = "ID: " + item.MONITOR_ID + " " + item.MONITOR_GYARTO + " " + item.MONITOR_NEV + " " + item.MONITOR_AR + " Ft "
                            + item.MONITOR_ATLO + "\" " + item.MONITOR_LEIRAS;
                        Console.WriteLine(fullMonitor);
                    }

                    break;

                    // List buyers
                case 2:
                    var buyers = (IQueryable<Data.Vasarlo>)logic.AllBuyers;
                    buyers.ToList();
                    foreach (var item in buyers)
                    {
                            Console.WriteLine(item.VASARLO_VEZETEKNEV + " " + item.VASARLO_KERESZTNEV + " " + item.VASARLO_EMAIL + " " + item.VASARLO_CIM + " 0" + item.VASARLO_TELEFON);
                    }

                    break;

                    // List orders
                case 3:
                    var orders = (IQueryable<Data.Rendeles>)logic.AllOrders;
                    orders.ToList();
                    foreach (var item in orders)
                    {
                        Console.WriteLine(item.MEGRENDELES_ID + " " + item.MEGRENDELES_IDEJE + " " + item.MEGRENDELT_MENNYISEG + "db " + item.MONITOR_GYARTO + " " + item.MONITOR_NEV);
                    }

                    break;
                default:
                    break;
            }
        }

        // Shows the menu
        private static void ShowMenu()
        {
            Console.WriteLine("Monitor Webshop");
            Console.WriteLine("*********************************************************************");
            Console.WriteLine("1 - List");
            Console.WriteLine("2 - Add");
            Console.WriteLine("3 - Delte");
            Console.WriteLine("4 - Modify");
            Console.WriteLine("5 - Complex queries");
            Console.WriteLine("0 - Exit");
            Console.WriteLine("*********************************************************************\n");
        }
    }
}
